# book-ssm

---

## database
The database uses Mysql, a total of 7 tables, 5 entity tables, and 2 relational tables.

| table_name | desc                              |
| ---------- | --------------------------------- |
| book       | book info table                   |
| reader     | reader info table                 |
| master     | master info table                 |
| tag        | book content classification table |
| publishing | publishing info table             |
| borrow     | borrow info table                 |
| book_tag   | books ang tags relationship table |

- book

| name          | type          | constraint                               | desc                          |
| ------------- | ------------- | ---------------------------------------- | ----------------------------- |
| book_id       | int           | primary key, unique, not null, auto inc. | book's id.                    |
| title         | varchar(128)  | not null                                 | book's title.                 |
| author        | varchar(32)   | None                                     | book's author                 |
| avatar        | varchar(256)  | None                                     | book's picture uri            |
| stock         | int           | not null, default 0                      | book's number which is in lib |
| price         | decimal(6, 2) | None                                     | boos's price                  |
| publishing_id | int           | not null, defualt 0                      | book's publishing house id    |
| create_time   | timestamp     | defualt current_timestamp                | data create time              |
| update_time   | timestamp     | defualt on update current_timestamp      | data last update time         |

- reader

| name      | type         | constraint                               | desc                    |
| --------- | ------------ | ---------------------------------------- | ----------------------- |
| reader_id | int          | primary key, unique, not null, auto inc. | reader's id             |
| name      | varchar(32)  | not null                                 | reader's real name      |
| username  | varchar(32)  | not null, unique                         | reader's login name     |
| password  | varchar(256) | not null                                 | reader's login password |
| avatar    | varchar(256) | None                                     | reader's picture uri    |
| age       | int          | None                                     | reader's age            |
| gender    | varchar(6)   | None                                     | reader's gender         |
| phone     | varchar(32)  | not null                                 | reader's phone number   |
| address   | varchar(256) | None                                     | reader's address        |

- master

| name      | type         | constraint                               | desc                    |
| --------- | ------------ | ---------------------------------------- | ----------------------- |
| master_id | int          | primary key, unique, not null, auto inc. | master's id             |
| username  | varchar(32)  | not null, unique                         | master's login name     |
| password  | varchar(256) | not null                                 | master's login password |
| name      | varchar(32)  | not null                                 | master's system name    |
| avatar    | varchar(256) | None                                     | master's picture uri    |

- tag

| name   | type        | constraint                               | desc       |
| ------ | ----------- | ---------------------------------------- | ---------- |
| tag_id | int         | primary key, unique, not null, auto inc. | tag's id   |
| name   | varchar(32) | not null, unique                         | tag's name |

- publishing

| name          | type         | constraint                               | desc                      |
| ------------- | ------------ | ---------------------------------------- | ------------------------- |
| publishing_id | int          | primary key, unique, not null, auto inc. | publishing's id           |
| name          | varchar(128) | not null, unique                         | publishing's name         |
| phone         | varchar(32)  | not null                                 | publishing's phone number |
| address       | varchar(256) | None                                     | publishing's address      |

- borrow

| name        | type      | constraint                               | desc             |
| ----------- | --------- | ---------------------------------------- | ---------------- |
| borrow_id   | int       | primary key, unique, not null, auto inc. | borrow's date id |
| book_id     | int       | not null                                 | book's id.       |
| reader_id   | int       | not null                                 | reader's id.     |
| create_time | timestamp | defualt current_timestamp                | data create time |
| return_time | timestamp | None                                     | return time      |

- book_tag

| name        | type | constrint                                | desc              |
| ----------- | ---- | ---------------------------------------- | ----------------- |
| relation_id | int  | primary key, unique, not null, auto inc. | relationship's id |
| book_id     | int  | not null                                 | book's id.        |
| tag_id      | int  | not null                                 | tag's id          |

-------

## view

...continue;

