package fun.qiutiandog.bookssm.controller;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.servlet.ModelAndView;

/**
 * e.g
 *
 * @author langxi.feng
 * @create 2022-07-19 20:27
 */
@Controller
public class TestController {

    @RequestMapping("/test")
    public ModelAndView test() {

        return new ModelAndView("/Hello");
    }
}
