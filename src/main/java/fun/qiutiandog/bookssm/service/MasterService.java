package fun.qiutiandog.bookssm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.qiutiandog.bookssm.entity.Master;

/**
 * @author Qiuti
 * @description 针对表【master(管理员表)】的数据库操作Service
 * @createDate 2022-07-19 18:25:52
 */
public interface MasterService extends IService<Master> {

}
