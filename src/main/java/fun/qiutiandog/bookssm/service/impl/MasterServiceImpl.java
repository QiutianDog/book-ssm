package fun.qiutiandog.bookssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.qiutiandog.bookssm.entity.Master;
import fun.qiutiandog.bookssm.mapper.MasterMapper;
import fun.qiutiandog.bookssm.service.MasterService;
import org.springframework.stereotype.Service;

/**
 * @author Qiuti
 * @description 针对表【master(管理员表)】的数据库操作Service实现
 * @createDate 2022-07-19 18:25:52
 */
@Service
public class MasterServiceImpl extends ServiceImpl<MasterMapper, Master>
        implements MasterService {

}




