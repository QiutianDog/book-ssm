package fun.qiutiandog.bookssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.qiutiandog.bookssm.entity.Tag;
import fun.qiutiandog.bookssm.mapper.TagMapper;
import fun.qiutiandog.bookssm.service.TagService;
import org.springframework.stereotype.Service;

/**
 * @author Qiuti
 * @description 针对表【tag(标签表)】的数据库操作Service实现
 * @createDate 2022-07-19 18:25:52
 */
@Service
public class TagServiceImpl extends ServiceImpl<TagMapper, Tag>
        implements TagService {

}




