package fun.qiutiandog.bookssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.qiutiandog.bookssm.entity.Publishing;
import fun.qiutiandog.bookssm.mapper.PublishingMapper;
import fun.qiutiandog.bookssm.service.PublishingService;
import org.springframework.stereotype.Service;

/**
 * @author Qiuti
 * @description 针对表【publishing(出版社表)】的数据库操作Service实现
 * @createDate 2022-07-19 18:25:52
 */
@Service
public class PublishingServiceImpl extends ServiceImpl<PublishingMapper, Publishing>
        implements PublishingService {

}




