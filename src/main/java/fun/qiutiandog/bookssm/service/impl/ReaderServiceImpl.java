package fun.qiutiandog.bookssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.qiutiandog.bookssm.entity.Reader;
import fun.qiutiandog.bookssm.mapper.ReaderMapper;
import fun.qiutiandog.bookssm.service.ReaderService;
import org.springframework.stereotype.Service;

/**
 * @author Qiuti
 * @description 针对表【reader(读者表)】的数据库操作Service实现
 * @createDate 2022-07-19 18:25:52
 */
@Service
public class ReaderServiceImpl extends ServiceImpl<ReaderMapper, Reader>
        implements ReaderService {

}




