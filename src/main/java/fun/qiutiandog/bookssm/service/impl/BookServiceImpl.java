package fun.qiutiandog.bookssm.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import fun.qiutiandog.bookssm.entity.Book;
import fun.qiutiandog.bookssm.mapper.BookMapper;
import fun.qiutiandog.bookssm.service.BookService;
import org.springframework.stereotype.Service;


/**
 * @author Qiuti
 * @description 针对表【book(图书表)】的数据库操作Service实现
 * @createDate 2022-07-19 18:25:52
 */
@Service
public class BookServiceImpl extends ServiceImpl<BookMapper, Book>
        implements BookService {

}




