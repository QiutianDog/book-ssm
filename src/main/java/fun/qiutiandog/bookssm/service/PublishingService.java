package fun.qiutiandog.bookssm.service;

import com.baomidou.mybatisplus.extension.service.IService;
import fun.qiutiandog.bookssm.entity.Publishing;

/**
 * @author Qiuti
 * @description 针对表【publishing(出版社表)】的数据库操作Service
 * @createDate 2022-07-19 18:25:52
 */
public interface PublishingService extends IService<Publishing> {

}
