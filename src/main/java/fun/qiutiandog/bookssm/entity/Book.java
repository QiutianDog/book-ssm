package fun.qiutiandog.bookssm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;
import java.math.BigDecimal;
import java.util.Date;

/**
 * 图书表
 *
 * @TableName book
 */
@TableName(value = "book")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Book implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = 394023603464432513L;

    /**
     * 图书ID
     */
    @TableId(type = IdType.AUTO)
    private Integer bookId;

    /**
     * 图书标题
     */
    private String title;

    /**
     * 图书作者
     */
    private String author;

    /**
     * 图书图片的服务器URL
     */
    private String avatar;

    /**
     * 图书库存余量
     */
    private Integer stock;

    /**
     * 图书单价
     */
    private BigDecimal price;

    /**
     * 出版社ID
     */
    private Integer publishingId;

    /**
     * 图书创建时间
     */
    private Date createTime;

    /**
     * 图书更新时间
     */
    private Date updateTime;
}