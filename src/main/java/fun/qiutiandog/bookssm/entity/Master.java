package fun.qiutiandog.bookssm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 管理员表
 *
 * @TableName master
 */
@TableName(value = "master")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Master implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = -847653594885410855L;

    /**
     * 管理员ID
     */
    @TableId(type = IdType.AUTO)
    private Integer masterId;

    /**
     * 管理员登录用的帐号
     */
    private String username;

    /**
     * 管理员密码（加密）
     */
    private String password;

    /**
     * 管理员用户名
     */
    private String name;

    /**
     * 管理员头像
     */
    private String avatar;


}