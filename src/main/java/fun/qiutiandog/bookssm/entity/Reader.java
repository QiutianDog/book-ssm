package fun.qiutiandog.bookssm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 读者表
 *
 * @TableName reader
 */
@TableName(value = "reader")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Reader implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = -5812285662894155615L;

    /**
     * 读者ID
     */
    @TableId(type = IdType.AUTO)
    private Integer readerId;

    /**
     * 读者真实姓名
     */
    private String name;

    /**
     * 读者登录用的账号
     */
    private String username;

    /**
     * 读者登录密码（加密）
     */
    private String password;

    /**
     * 读者头像
     */
    private String avatar;

    /**
     * 读者年龄
     */
    private Integer age;

    /**
     * 读者性别
     */
    private String gender;

    /**
     * 读者电话
     */
    private String phone;

    /**
     * 读者地址
     */
    private String address;


}