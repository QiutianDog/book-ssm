package fun.qiutiandog.bookssm.entity;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.io.Serializable;

/**
 * 出版社表
 *
 * @TableName publishing
 */
@TableName(value = "publishing")
@Data
@AllArgsConstructor
@NoArgsConstructor
public class Publishing implements Serializable {

    @TableField(exist = false)
    private static final long serialVersionUID = -3611123624781048129L;

    /**
     * 出版社ID
     */
    @TableId(type = IdType.AUTO)
    private Integer publishingId;

    /**
     * 出版社名称
     */
    private String name;

    /**
     * 出版社联系电话
     */
    private String phone;

    /**
     * 出版社地址
     */
    private String address;

}