package fun.qiutiandog.bookssm.config;

import org.springframework.context.annotation.Configuration;

/**
 * 业务配置
 *
 * @author langxi.feng
 * @create 2022-07-19 20:35
 */
@Configuration
public class RootConfiguration {
}
