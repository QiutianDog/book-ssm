package fun.qiutiandog.bookssm.config;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;
import org.springframework.web.servlet.config.annotation.DefaultServletHandlerConfigurer;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurer;
import org.thymeleaf.spring5.SpringTemplateEngine;
import org.thymeleaf.spring5.templateresolver.SpringResourceTemplateResolver;
import org.thymeleaf.spring5.view.ThymeleafViewResolver;
import org.thymeleaf.templateresolver.ITemplateResolver;

/**
 * e.g
 *
 * @author langxi.feng
 * @create 2022-07-20 09:33
 */
@Configuration
@ComponentScan("fun.qiutiandog.bookssm.controller")
public class WebConfiguration implements WebMvcConfigurer {
    /**
     * 我们需要使用ThymeleafViewResolver作为视图解析器，并解析我们的HTML页面
     */
    @Bean
    public ThymeleafViewResolver thymeleafViewResolver(@Autowired SpringTemplateEngine springTemplateEngine) {
        ThymeleafViewResolver resolver = new ThymeleafViewResolver();
        // 可以存在多个视图解析器，并且可以为他们设定解析顺序
        resolver.setOrder(1);
        resolver.setCharacterEncoding("UTF-8");
        // 和之前JavaWeb阶段一样，需要使用模板引擎进行解析，所以这里也需要设定一下模板引擎
        resolver.setTemplateEngine(springTemplateEngine);
        return resolver;
    }

    /**
     * 配置模板解析器
     */
    @Bean
    public SpringResourceTemplateResolver templateResolver() {
        SpringResourceTemplateResolver resolver = new SpringResourceTemplateResolver();
        // 需要解析的后缀名称
        resolver.setSuffix(".html");
        // 需要解析的HTML页面文件存放的位置
        resolver.setPrefix("/WEB-INF/template");
        return resolver;
    }

    /**
     * 配置模板引擎Bean
     */
    @Bean
    public SpringTemplateEngine springTemplateEngine(@Autowired ITemplateResolver resolver) {
        SpringTemplateEngine engine = new SpringTemplateEngine();
        // 模板解析器，默认即可
        engine.setTemplateResolver(resolver);
        return engine;
    }

    /**
     * 开起默认的servlet
     * @param configurer configurer
     */
    @Override
    public void configureDefaultServletHandling(DefaultServletHandlerConfigurer configurer) {
        configurer.enable();
    }

    /**
     * 添加资源文件访问
     * @param registry registry
     */
    @Override
    public void addResourceHandlers(ResourceHandlerRegistry registry) {
        registry.addResourceHandler("/static/**")
                .addResourceLocations("/WEB-INF/static/");
    }

}
