package fun.qiutiandog.bookssm.config;

import org.springframework.web.servlet.support.AbstractAnnotationConfigDispatcherServletInitializer;

/**
 * 初始化配置 用DispatcherServlet替代Tomcat自带的Servlet
 *
 * @author langxi.feng
 * @create 2022-07-19 20:34
 */
public class MainConfigInitializer extends AbstractAnnotationConfigDispatcherServletInitializer {
    /**
     * 基本的Spring配置类，一般用于业务层配置
     *
     * @return class
     */
    @Override
    protected Class<?>[] getRootConfigClasses() {
        return new Class[]{RootConfiguration.class};
    }

    /**
     * 配置DispatcherServlet的配置类、主要用于Controller等配置
     *
     * @return class
     */
    @Override
    protected Class<?>[] getServletConfigClasses() {
        return new Class[]{WebConfiguration.class};
    }

    /**
     * 匹配路径
     *
     * @return string
     */
    @Override
    protected String[] getServletMappings() {
        return new String[]{"/"};
    }
}
