package fun.qiutiandog.bookssm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.qiutiandog.bookssm.entity.Book;

/**
 * @author Qiuti
 * @description 针对表【book(图书表)】的数据库操作Mapper
 * @createDate 2022-07-19 18:25:52
 * @Entity fun.qiutiandog.bookssm.entity.Book
 */
public interface BookMapper extends BaseMapper<Book> {

}




