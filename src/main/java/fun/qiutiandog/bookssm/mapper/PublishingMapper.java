package fun.qiutiandog.bookssm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.qiutiandog.bookssm.entity.Publishing;

/**
 * @author Qiuti
 * @description 针对表【publishing(出版社表)】的数据库操作Mapper
 * @createDate 2022-07-19 18:25:52
 * @Entity fun.qiutiandog.bookssm.entity.Publishing
 */
public interface PublishingMapper extends BaseMapper<Publishing> {

}




