package fun.qiutiandog.bookssm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.qiutiandog.bookssm.entity.Tag;

/**
 * @author Qiuti
 * @description 针对表【tag(标签表)】的数据库操作Mapper
 * @createDate 2022-07-19 18:25:52
 * @Entity fun.qiutiandog.bookssm.entity.Tag
 */
public interface TagMapper extends BaseMapper<Tag> {

}




