package fun.qiutiandog.bookssm.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import fun.qiutiandog.bookssm.entity.Master;

/**
 * @author Qiuti
 * @description 针对表【master(管理员表)】的数据库操作Mapper
 * @createDate 2022-07-19 18:25:52
 * @Entity fun.qiutiandog.bookssm.entity.Master
 */
public interface MasterMapper extends BaseMapper<Master> {

}




